import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.http.options.root = '/api'
// 配置使用formDate
Vue.http.options.emulateHTTP = true
Vue.http.options.emulateJSON = true

const httpUrl = 'http://39.105.17.99:8080/'
function httpGet (url, params) {
  return new Promise((resolve, reject) => {
    Vue.http.get(this.httpUrl + url, params).then(
      (res) => {
        resolve(res.json())
      },
      (err) => {
        reject(err.json())
      }
    )
  })
}

function httpPost (url, params) {
  return new Promise((resolve, reject) => {
    Vue.http.post(this.httpUrl + url, params).then(
      (res) => {
        resolve(res.json())
      },
      (err) => {
        reject(err.json())
      }
    )
  })
}

Vue.filter(
  'dateFilter', (value) => {
    console.log(value, '时间转换')
    // 注意这里必须是个数字，要不然报错
    var dateString = new Date(value - 0)
    return dateString
  }
)
export default {
  httpUrl,
  httpGet,
  httpPost
}
