/* 引入vue */
import Vue from 'vue'
import App from './App'

/* 导入路由 */
import router from './router'

// 引入 vue-beauty UI框架
import VueBeauty from 'vue-beauty'
import 'vue-beauty/package/style/vue-beauty.min.css'

// 引入iView框架 UI框架
import iView from 'iview'
import 'iview/dist/styles/iview.css'
// 公共css
import './common/common.css'

// 引入时间处理moment.js
import VueMoment from 'vue-moment'

// 导入全局变量和方法，这里包括http请求的封装
import global from './common/common'
Vue.prototype.GLOBAL = global

Vue.use(VueMoment)
Vue.use(VueBeauty)
Vue.use(iView)

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
    App
  },
  template: '<App/>',
  http: { // 配置http信息
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  }
})
